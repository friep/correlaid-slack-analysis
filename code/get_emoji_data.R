# SLACK ANALYSIS ---------------------------------------------------------------

library(readr)
library(rvest)
library(httr)
library(stringr)
library(purrr)
library(jsonlite)
library(dplyr)
library(tidyr)

rm(list = ls())
source("code/slack_functions.R")

SLACK_TOKEN <- read_lines("data/slack_key.txt")
DATA_PATH <- "data/channel_data"

# GET DATA FROM API ---------------------------------------------------------

chs <- get_channel_list(slack_token = SLACK_TOKEN)
chs <- chs$channels

params <- map_df(chs, `[`, c("id", "name"))

params %>% 
  pwalk(get_history, slack_token = SLACK_TOKEN, count = 1000, data_path = DATA_PATH)

rm(SLACK_TOKEN, chs)

# READ IN ALL MESSAGES----------------------------------------------------------
files <- list.files(DATA_PATH, full.names = TRUE)

all_channels <- files %>% 
  map(read_channel_rds, data_path = DATA_PATH) %>% 
  keep(~is.data.frame(.x)) # where we have no messages, we don't get a data frame

# bind together and keep only relevant variables
all_messages <- all_channels %>% 
  bind_rows() %>% 
  select(ts, channel_id, user, bot_id, type, subtype, text, reactions)

rm(all_channels)

# REACTIONS --------------------------------------------------------------------
# only keep those messages to which we have reactions to 
all_reactions <- all_messages %>%
  mutate(has_react = reactions %>%
           map(function(x) !is.null(x))) %>%
  filter(has_react == TRUE)

# unnest the reactions variable, i.e. pull out the data inside each row and 
# create a row for each entry in the row-dataframe
all_reactions <- all_reactions %>%
  mutate(ts = as_vector(ts)) %>% # timestamp as vector
  group_by(channel_id, ts) %>% 
  unnest(reactions)

# now do the same for the user variable
# but first we have to convert the matrix in each variable to a little tibble 
all_reactions <- all_reactions %>%
  mutate(name = as_vector(name)) %>% # name as vector
  mutate(user = users %>%
           map(function(u) tibble(user = as.character(u)))) %>%
  group_by(channel_id, ts, name) %>%
  unnest(user) %>% 
  ungroup() # remove grouping variables

# rename some variables
all_reactions <- all_reactions %>% 
  select(channel_id, ts, reacting_user = user, emoji_name = name)

str(all_reactions)

write_csv(all_reactions, "data/all_reactions.csv")

# emoji counts
emoji_counts <- all_reactions %>% 
  group_by(emoji_name) %>% 
  summarize(count = n())

emoji_counts$emoji <- emoji_counts$emoji_name %>% 
  map_chr(possibly(function(e) emo::ji(e), otherwise = "???"))

emoji_counts <- emoji_counts %>% 
  select(emoji, emoji_name, count) %>% 
  arrange(-count)

write_rds(emoji_counts, "data/emoji_counts_correlaid.rds")
